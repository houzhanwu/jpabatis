/**
 *    Copyright 2009-2018 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.yinsin.jpabatis.executor;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;

import com.yinsin.jpabatis.config.Configuration;
import com.yinsin.jpabatis.mapper.BoundSql;
import com.yinsin.jpabatis.mapper.MappedStatement;
import com.yinsin.jpabatis.session.ResultHandler;
import com.yinsin.jpabatis.session.RowBounds;

/**
 * @author Jeff Butler
 */
public class BatchExecutor extends BaseExecutor {

	public static final int BATCH_UPDATE_RETURN_VALUE = Integer.MIN_VALUE + 1002;

	private final List<Statement> statementList = new ArrayList<>();
	private final List<BatchResult> batchResultList = new ArrayList<>();
	private String currentSql;
	private MappedStatement currentStatement;

	public BatchExecutor(Configuration configuration, EntityManager transaction) {
		super(configuration, transaction);
	}

	@Override
	public <E> E doQuery(MappedStatement ms, Object parameter, ResultHandler<?> resultHandler, BoundSql boundSql) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E> List<E> doQueryList(MappedStatement ms, Object parameter, ResultHandler<?> resultHandler, BoundSql boundSql) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E> Page<E> doQueryList(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler<?> resultHandler, BoundSql boundSql) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}


}
