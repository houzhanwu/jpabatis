package com.yinsin.jpabatis.mapper;

public enum ParameterMode {
	IN, OUT, INOUT
}
